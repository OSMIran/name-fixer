import psycopg2
from config import config
import overpass
import geojson
import re
from datetime import datetime
from jinja2 import Environment, FileSystemLoader

TEMPLATE_FILE_NAME = 'make-site.html'

projectName = "Shahid_Fixer"
pathOriginalFile = "original/"
pathFixFile = "fixed/"
pathLog = "logs/"

createFileTime = datetime.now().strftime('%Y-%m-%d')

api = overpass.API(url="https://overpass.kumi.systems/api/interpreter") # Costum URL


def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

	# execute a statement
        sql = '''select osm_id from shahid'''
        cur.execute(sql)
        global nameFetch
        nameFetch = cur.fetchall()
        print("Running SQL Query ...")

	# close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
    return nameFetch


def fix_data(nameFetch):
    new_tup = []
    for num in nameFetch:
        new_tup.append(str(*num))
    
    string_tup = ",".join(new_tup)
    
    print("Running overpass query ...\nThis action may be will take time to getting data from OSM ...")
    
    Result = api.get(f"""area["ISO3166-1"="IR"]->.boundaryarea;
     (
      way(id:{string_tup})(area.boundaryarea);
     );
    (._;>;);
    """, verbosity="geom", responseformat="geojson")
    
    print('Done!')
    
    
    print(f"\nSaving original file in '{pathOriginalFile}{projectName}-{createFileTime}.geojson'")
    
    with open(f"{pathOriginalFile}{projectName}-{createFileTime}.geojson", "w", encoding="utf-8") as f:
        geojson.dump(Result, f, ensure_ascii=False, indent=4)
    
    orig_new_names = {}
    values = []
    
    pattern = r"(^شهید)([^ ])"
    
    with open(f"{pathOriginalFile}{projectName}-{createFileTime}.geojson", "r", encoding='utf-8') as f:
        originalLoad = geojson.load(f)
        for idx, i in enumerate(originalLoad['features']):
            originalDataName = i.get('properties').get('name')
            originalDataType = i.get('geometry').get('type')
            originalDataiD = i.get('id')
            if originalDataName is not None:
                if (re.findall(re.compile(pattern), originalDataName)):
                    newDataName = re.sub(pattern, "\g<1> \g<2>", originalDataName)
                    values.append({
                        "originalDataName": originalDataName,
                        "originalDataType": getDataType(originalDataType),
                        "originalDataTypeShort": getDataType(originalDataType, True),
                        "originalDataiD": originalDataiD,
                        "newDataName": newDataName,
                        "createFileTime": createFileTime
                    })
                    originalLoad['features'][idx]['properties']['name'] = newDataName
                    orig_new_names[originalDataName] = newDataName
    
                # originalLoad['features'][idx]['properties']["action"] = "modify" # TODO
    
    print(f"\nSaving fix file in '{pathFixFile}{projectName}-{createFileTime}.geojson'")
    with open(f"{pathFixFile}{projectName}-{createFileTime}.geojson", "w", encoding="utf-8") as f:
        geojson.dump(originalLoad, f, ensure_ascii=False, indent=4)
    return values


def parse_html(template_name: str, values: list):
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template(template_name)

    output_from_parsed_template = template.render(data=values)

    # to save the results
    with open(f"{pathLog}{projectName}-{createFileTime}.html", "w") as fh:
        fh.write(output_from_parsed_template)


def getDataType(data_type, short=False):
    return{
        "LineString": ["way", "w"],
        "Node": ["node", "n"],
        "Relation": ["relation", "r"]
    }[data_type][1 if short else 0]

if __name__ == '__main__':
    name_fetch = connect()
    values = fix_data(name_fetch)
    parse_html(TEMPLATE_FILE_NAME, values)
