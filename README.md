# Name Fixer

```sh
git clone git@gitlab.com:OSMIran/name-fixer.git
cd name-fixer
python3 -m venv .venv && source .venv/bin/activate
pip3 install -r ./requirements.txt
python3 [example: shahid-fixer.py]
```